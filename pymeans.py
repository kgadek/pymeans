#!/usr/bin/env python3.2

import random as rnd
import point as p
from math import sqrt
from itertools import combinations


# Inicjujemy generatora
rnd.seed()


class Cluster(object):
    def __init__(self, c, xs, var):
        self.  c = c    # Centroid
        self. xs = xs   # Punkty
        self.var = var  # Wariancja

    def __repr__(self):
        return "{∀x∈ %s: x≡c;  σ=%.1f}" % (self.xs, self.var)


def ymeans(pop, k=None):
    """Algorytm Y-Means"""

    class Reclusterize(Exception):
        pass

    class Recombine(Exception):
        pass

    # Ilość podziałów
    k = k if k else rnd.randint(1, len(pop))
    print("k=%d" % k)
    # Wymieszanie populacji
    rnd.shuffle(pop)  # !pop

    ### Podziel na klastry

    # Wybierz początkowe centroidy
    cs = dict([(k, ([], 0)) for k in pop[:k]])
    ctail = pop[k:]               # Pozostali "kandydaci" na centroidy
    while True:                   # Prawie idiom --- powtarzamy tyle razy ile
        try:                      # trzeba. Jak wyjątek to znaczy,
                                  # że powtarzamy.
            print("Główna pętla-----------------------------------------------")
            print("|  |cs| = %-3d----------------------------------------------" % len(cs))
            for k in cs:
                cs[k] = ([], 0)  # Czyścimy podział
            pop2clusters(pop, cs)  # !cs

            ### Sprawdzamy, czy którykolwiek klaster jest pusty

            for k in (c for c in cs if len(cs[c][0]) < 2):
                if len(cs[k][0]) == 1 and not cs[k][0][0] is k:
                    continue  # Jest ok, zawiera jeden element
                              # (ale nie samego siebie)
                print("|  Klaster %-15s jest pusty----------------------" % k)
                ## Zbyt mały klaster, wybieramy nowy centroid (losowo)
                del cs[k]
                if len(ctail) > 0:
                    cs[ctail[0]] = ([], 0)
                    ctail = ctail[1:]
                raise Reclusterize

            ### Wyznaczanie nowych centroidów a także ew. ich dzielenie

            print("|  Liczenie nowych centroidów------------------------------")
            for c in dict(cs):
                cm = p.Point.average([x[0] for x in cs[c][0]])  # Nowy centroid
                # Nowy zestaw, obiekty posortowane
                # wg. odległości od nowego centroidu rosnąco
                xds = sorted([(x[0], cm.dist(x[0])) for x in cs[c][0]],
                             key=lambda xd: -xd[1])
                # Wyznacz odchylenie standardowe odległości
                (dmean, dstdv) = meanStdv((xd[1] for xd in xds))
                # Stary → nowy klaster
                del cs[c]
                cs[cm] = (xds, 2.32 * dstdv)
                if xds[0][1] > 2.32 * dstdv:
                    print("|  C %10s →%10s σ=%3.1f ma outs: %-10s-----" % (c, cm, dstdv, xds[0][0]))
                    print("|    xds = %s" % xds)
                    # Punkt poza 'confident area' więc jako nowy klaster
                    cs[xds[0][0]] = ([], 0)
                    raise Reclusterize

            ### Łączenie

            while True:
                try:
                    for k1, k2 in combinations(dict(cs), 2):
                        # Jeśli odległość m. centroidami jest <= 2.32 odchy. std
                        # grupy punktów ich otaczających, to łączymy klastry.
                        if k1.dist(k2) <= cs[k1][1] + cs[k2][1]:
                            print("|  %10s → %10s < %-3.1f---------------------------" % (k1, k2, cs[k1][1] + cs[k2][1]))
                            # Łączymy punkty, nie ma znaczenia kolejność
                            xds = cs[k1][0] + cs[k2][0]
                            cm = p.Point.average([k1, k2])  # Nowy centroid
                            (mean, stdv) = meanStdv((xd[1] for xd in xds))
                            del cs[k1]
                            del cs[k2]
                            cs[cm] = (xds, stdv)
                            raise Recombine
                except Recombine:
                    pass
                else:
                    break

        except Reclusterize:
            pass       # Pętla jeszcze raz
        else:
            return cs  # Wszystko ok, koniec tej iteracji


def pop2clusters(xs, cs):
    for x in xs:
        (dd, cc, xx) = min(((x.dist(c), c, x) for c in cs), key=lambda a: a[0])
        cs[cc][0].append((xx, dd))


def meanStdv(xs):
    (m, s) = (0, 0)
    cnt = 0
    for k, x in enumerate(xs, start=1):
        (m, m_old) = (m + (x - m) / k, m)
        s = s + (x - m_old) * (x - m)
        cnt = k
    return (m, sqrt(max(0, s / (cnt - 1))) if cnt > 1 else 0)
