def getData():
    """Tu wczytywanie danych z pliku czy z czegokolwiek..."""
    return [Euclid(x, y) for (x, y) in [(0, 0), (2, 3), (1, 1), (0, 0), (4, 3), (3, 4)]]


class Euclid (object):
    def __init__(self, x, y):
        super(Euclid, self).__init__()
        self.x = x
        self.y = y

    def __repr__(self):
        """To po to, by ew. wyświetlanie działało. Zrób jak będzie Ci się chciało."""
        return "E(%.1f,%.1f)" % (self.x, self.y)

    def dist(p1, p2):
        """To, co będzie potrzebne, to:
          * x.dist(y) >= 0
          * x.dist(y) == y.dist(x)
          * x.dist(y) == 0 <=> x == y
          * x.dist(y) + y.dist(z) >= x.dist(z)"""
        return (p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2

    def mean(es):
        """Wyznaczenie średniej arytmetycznej punktów."""
        m = es[0]
        for k, e in enumerate(es[1:], start=2):
            m = m.add(e.add(m.inv()).div(k))
        return m

    # Takie tam helpery
    def inv(self):
        return Euclid(-self.x, -self.y)

    def add(self, e):
        return Euclid(self.x + e.x, self.y + e.y)

    def div(self, k):
        return Euclid(self.x / k, self.y / k)
