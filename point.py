#!//usr/bin/python3.2

from math import sqrt


class Point (object):
  
  
  mins = []
  maxs = []
  types = ['continuous', 'discrete', 'discrete', 'discrete', 'continuous', 'continuous', 'discrete', 'continuous', 'continuous', 'continuous', 'continuous', 'discrete', 'continuous', 'discrete', 'discrete', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'discrete', 'discrete', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'continuous', 'discrete']
  dim = 41
  
  @staticmethod
  def getData(filename):
    """Tu wczytywanie danych z pliku czy z czegokolwiek..."""
    # open file
    file = open(filename, 'r')
    # preparing
    Point.mins = []
    Point.maxs = []
    for type in Point.types:
      if type == 'discrete':
        Point.mins.append({})
      else:
        Point.mins.append(None)
      Point.maxs.append(None)
    # stage 1 - loading into memory
    lines = []
    for line in file:
      line = line.strip().split(',')
      line[-1] = line[-1].rstrip('.')
      for i,elm,min,max in zip(range(len(Point.types)), line, Point.mins, Point.maxs):
        if Point.types[i] == 'discrete':
          if not elm in min:
            Point.mins[i][elm] = len(Point.mins[i])
          elm = Point.mins[i][elm]
        else:
          elm = float(elm)
          if min == None or min > elm:
            Point.mins[i] = elm
          if max == None or max < elm:
            Point.maxs[i] = elm
        line[i] = elm
      lines.append(line)
    # stage 2 - recalculating
    for li, line in zip(range(len(lines)), lines):
      for i, elm, type, min, max in zip(range(len(Point.types)), line, Point.types, Point.mins, Point.maxs):
        if type == 'continuous' and (max - min) != 0.0:
          line[i] = (elm - min) / (max - min)
      lines[li] = line
    # swaping dicts
    for i, type in zip(range(len(Point.types)), Point.types):
      if type == 'discrete':
        Point.mins[i] = dict(zip(Point.mins[i].values(), Point.mins[i].keys()))
    return [Point(line) for line in lines]
  
  def __init__(self, input):
    self.data = input
  
  def __repr__(self):
    """To po to, by ew. wyświetlanie działało. Zrób jak będzie Ci się chciało."""
    output = []
    for elm, type, min, max in zip(self.data, Point.types, Point.mins, Point.maxs):
      if type == 'discrete':
        output.append(min[elm])
      else:
        output.append(str(elm * (max - min) + min))
    return ','.join(output)
  
  @staticmethod
  def _moda(values):
    counts = dict()
    for value in values:
      if not value in counts:
        counts[value] = 0
      counts[value] += 1
    maxcount = 0
    maxkey = None
    for key, value in counts.items():
      if value > maxcount:
        maxkey = key
        maxcount = value
    return maxkey
  
  @staticmethod
  def _median(values):
    values.sort()
    return values[len(values) // 2]
  
  @staticmethod
  def average(points):
    if len(points) < 1:
      return None
    new = []
    for type, i in zip(Point.types, range(Point.dim)):
      if type == 'discrete':
        new.append([])
      else:
        new.append(0.0)
    for point in points:
      for i, type, value in zip(range(Point.dim), Point.types, point.data):
        if type == 'discrete':
          new[i].append(value)
        else:
          new[i] += value;
    size = len(points);
    for type, i in zip(Point.types, range(Point.dim)):
      if type == 'discrete':
        new[i] = Point._moda(new[i])
      else:
        new[i] = new[i] / size
    return Point(new)
    
  def dist(p1, p2):
    """To, co będzie potrzebne, to:
      * x.dist(y) >= 0
      * x.dist(y) == y.dist(x)
      * x.dist(y) == 0 <=> x == y
      * x.dist(y) + y.dist(z) >= x.dist(z)"""
    sum = 0.0
    for i, a, b in zip(range(Point.dim), p1.data, p2.data):
      if type(a) is float:
        sum += (a - b)**2
      else:
        if a != b:
          sum += 1.0
    return sqrt(sum)

#getData('../kddcup.data')
ls=Point.getData('100.data')
for i, elm in zip(range(len(ls)), ls):
  print(elm)
  print(ls[0].dist(elm))
  print()
  
print(Point.average(ls[0:3]))
print()
print(Point.average(ls[0:7]))
print()